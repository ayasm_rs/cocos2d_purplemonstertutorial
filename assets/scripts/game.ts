import Star from "./star";

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class Game extends cc.Component {


    // this property quotes the PreFab resource of stars
    @property({
        type: cc.Prefab
    })
    starPrefab = null;

    // the random scale of disappearing time for stars
    @property
    maxStarDuration: number = 0;
    @property
    minStarDuration: number = 0;

    //Label to display score
    @property({
        type: cc.Label
    })
    scoreDisplay = null;

    /**
     * common parameters:
     * default：Sets the default value for the property, which is only used when the component is first added to the node
     * type：To qualify the data type of a property, see CCClass Advanced Reference: type attribute
     * visible：Set to false to not display this property in the Properties
     * serializable： Set to False to not serialize (save) this property
     * displayName：Display in the Properties as the specified name
     * tooltip：To add a property's tooltip to the Properties
     */

    // ground node for confirming the height of the generated star's position
    @property({
        type: cc.Node
    })
    ground = null;

    // player node for obtaining the jump height of the main character and controlling the movement switch of the main character
    @property({
        type: cc.Node
    })
    player = null;

    @property({
        type:cc.AudioClip
    })
    scoreAudio=null;

    groundY: any;

    score: number;
    timer: number;
    starDuration: number;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        // initialize timer
        this.timer = 0;
        this.starDuration = 0;
        // obtain the anchor point of ground level on the y axis
        this.groundY = this.ground.y + this.ground.height / 2; // this.ground.top may also work
        // initialize scoring
        this.score = 0;
        // generate a new star
        this.spawnNewStar();
    }


    spawnNewStar() {
        // generate a new node in the scene with a preset template
        var newStar: cc.Node = cc.instantiate(this.starPrefab);
        // put the newly added node under the Canvas node
        this.node.addChild(newStar);
        // set up a random position for the star
        newStar.setPosition(this.getNewStarPosition());
        newStar.getComponent(Star).game = this;
        // reset timer, randomly choose a value according the scale of star duration
        this.starDuration = this.minStarDuration + Math.random() * (this.maxStarDuration - this.minStarDuration);
        this.timer = 0;

    }

    getNewStarPosition() {
        var randX = 0;
        var randY = 0;

        // According to the position of the ground level and the main character's jump height, randomly obtain an anchor point of the star on the y axis        
        if (this.player.getComponent('Player') != null) {
            randY = this.groundY + Math.random() * this.player.getComponent('Player').jumpHeight + 50;
        }


        // according to the width of the screen, randomly obtain an anchor point of star on the x axis
        var maxX = this.node.width / 2;
        randX = (Math.random() - 0.5) * 2 * maxX;
        // return to the anchor point of the star
        return cc.v2(randX, randY);
    }

    gainScore() {
        this.score += 1;
        // update the words of the scoreDisplay Label
        this.scoreDisplay.string = 'Score: ' + this.score;
        // play the scoring sound effect
        cc.audioEngine.playEffect(this.scoreAudio, false);
    }

    start() {

    }

    update (dt) {
         // update timer for each frame, when a new star is not generated after exceeding duration
        // invoke the logic of game failure
        if (this.timer > this.starDuration) {
            this.gameOver();
            return;
        }
        this.timer += dt;
    }
    gameOver() {
        this.scoreDisplay.string="Game Over!!!!";
        this.player.stopAllActions(); //stop the jumping action of the player node
        cc.director.loadScene('game');
    }
}

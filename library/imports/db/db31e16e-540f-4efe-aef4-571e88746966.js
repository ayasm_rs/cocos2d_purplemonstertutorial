"use strict";
cc._RF.push(module, 'db31eFuVA9O/q70Vx6IdGlm', 'game');
// scripts/game.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var star_1 = require("./star");
// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Game = /** @class */ (function (_super) {
    __extends(Game, _super);
    function Game() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        // this property quotes the PreFab resource of stars
        _this.starPrefab = null;
        // the random scale of disappearing time for stars
        _this.maxStarDuration = 0;
        _this.minStarDuration = 0;
        //Label to display score
        _this.scoreDisplay = null;
        /**
         * common parameters:
         * default：Sets the default value for the property, which is only used when the component is first added to the node
         * type：To qualify the data type of a property, see CCClass Advanced Reference: type attribute
         * visible：Set to false to not display this property in the Properties
         * serializable： Set to False to not serialize (save) this property
         * displayName：Display in the Properties as the specified name
         * tooltip：To add a property's tooltip to the Properties
         */
        // ground node for confirming the height of the generated star's position
        _this.ground = null;
        // player node for obtaining the jump height of the main character and controlling the movement switch of the main character
        _this.player = null;
        _this.scoreAudio = null;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    Game.prototype.onLoad = function () {
        // initialize timer
        this.timer = 0;
        this.starDuration = 0;
        // obtain the anchor point of ground level on the y axis
        this.groundY = this.ground.y + this.ground.height / 2; // this.ground.top may also work
        // initialize scoring
        this.score = 0;
        // generate a new star
        this.spawnNewStar();
    };
    Game.prototype.spawnNewStar = function () {
        // generate a new node in the scene with a preset template
        var newStar = cc.instantiate(this.starPrefab);
        // put the newly added node under the Canvas node
        this.node.addChild(newStar);
        // set up a random position for the star
        newStar.setPosition(this.getNewStarPosition());
        newStar.getComponent(star_1.default).game = this;
        // reset timer, randomly choose a value according the scale of star duration
        this.starDuration = this.minStarDuration + Math.random() * (this.maxStarDuration - this.minStarDuration);
        this.timer = 0;
    };
    Game.prototype.getNewStarPosition = function () {
        var randX = 0;
        var randY = 0;
        // According to the position of the ground level and the main character's jump height, randomly obtain an anchor point of the star on the y axis        
        if (this.player.getComponent('Player') != null) {
            randY = this.groundY + Math.random() * this.player.getComponent('Player').jumpHeight + 50;
        }
        // according to the width of the screen, randomly obtain an anchor point of star on the x axis
        var maxX = this.node.width / 2;
        randX = (Math.random() - 0.5) * 2 * maxX;
        // return to the anchor point of the star
        return cc.v2(randX, randY);
    };
    Game.prototype.gainScore = function () {
        this.score += 1;
        // update the words of the scoreDisplay Label
        this.scoreDisplay.string = 'Score: ' + this.score;
        // play the scoring sound effect
        cc.audioEngine.playEffect(this.scoreAudio, false);
    };
    Game.prototype.start = function () {
    };
    Game.prototype.update = function (dt) {
        // update timer for each frame, when a new star is not generated after exceeding duration
        // invoke the logic of game failure
        if (this.timer > this.starDuration) {
            this.gameOver();
            return;
        }
        this.timer += dt;
    };
    Game.prototype.gameOver = function () {
        this.scoreDisplay.string = "Game Over!!!!";
        this.player.stopAllActions(); //stop the jumping action of the player node
        cc.director.loadScene('game');
    };
    __decorate([
        property({
            type: cc.Prefab
        })
    ], Game.prototype, "starPrefab", void 0);
    __decorate([
        property
    ], Game.prototype, "maxStarDuration", void 0);
    __decorate([
        property
    ], Game.prototype, "minStarDuration", void 0);
    __decorate([
        property({
            type: cc.Label
        })
    ], Game.prototype, "scoreDisplay", void 0);
    __decorate([
        property({
            type: cc.Node
        })
    ], Game.prototype, "ground", void 0);
    __decorate([
        property({
            type: cc.Node
        })
    ], Game.prototype, "player", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], Game.prototype, "scoreAudio", void 0);
    Game = __decorate([
        ccclass
    ], Game);
    return Game;
}(cc.Component));
exports.default = Game;

cc._RF.pop();